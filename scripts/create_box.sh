#!/bin/bash

FILE_LOCATION=`dirname $0`

for file in ${FILE_LOCATION}/../samples/*.tif
do
    tesseract $file ${FILE_LOCATION}/../samples/`basename $file .tif` batch.nochop makebox
done
