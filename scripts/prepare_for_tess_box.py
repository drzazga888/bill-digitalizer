#!/usr/bin/env python3

import fnmatch
import os
import re
from PIL import Image

IMG_ROOT = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../bills/')
IMG_GLOBS = ['*_slip_*']
MIN_HEIGHT = 25
REGEX_MATCH_TYPES = re.compile(r"^.*/(.+?)_(.+?)_")
OUTPUT_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../samples/')
TRESHOLD = 128

valid_sample_counter = {}

def prepare(file_path):
    global valid_sample_index
    im = Image.open(file_path)
    # check height
    if im.size[1] < MIN_HEIGHT:
        print('{} skipped: height is to small ({}px, but min={}px)'.format(
            file_path,
            im.size[1],
            MIN_HEIGHT
        ))
        return
    # extract shop (dunno why)
    m = REGEX_MATCH_TYPES.match(file_path)
    if not m:
        print('{} skipped: cannot extract type from file name'.format(
            file_path,
        ))
        return
    shop = m[2]
    valid_sample_counter.setdefault(shop, 0)
    #print(shop, valid_sample_counter[shop])
    gray = im.convert('L')
    bw = gray.point(lambda x: 0 if x<TRESHOLD else 255, '1')
    bw.save(os.path.join(OUTPUT_FOLDER, 'pl.receipt_{}.exp{}.tif'.format(shop, valid_sample_counter[shop])), dpi=(72,72))
    valid_sample_counter[shop] += 1

if __name__ == '__main__':
    for root, dirnames, filenames in os.walk(IMG_ROOT):
        for img_glob in IMG_GLOBS:
            for filename in fnmatch.filter(filenames, img_glob):
                prepare(os.path.join(root, filename))
