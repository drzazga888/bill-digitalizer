#!/bin/bash

FILE_LOCATION=`dirname $0`

for file in ${FILE_LOCATION}/../samples/*.tif
do
    echo "$file"
    tesseract $file ${FILE_LOCATION}/../samples/`basename $file .tif` box.train
    echo
done
