#!/usr/bin/env python3

import fnmatch
import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

IMG_ROOT = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../bills/')
IMG_GLOB = 'prod_*.jpg'

def get_cuts_mid_tresh(sig):
    per95 = max(sig) * 0.95
    treshold = [x >= per95 for x in sig]
    peaks = []
    peak_start = None
    for i in range(len(treshold) - 1):
        if peak_start is None and not treshold[i] and treshold[i+1]:
            peak_start = i
        elif peak_start is not None and treshold[i] and not treshold[i+1]:
            peaks.append(int((peak_start + i) / 2))
            peak_start = None
    return peaks

def split_to_lines(file_path):
    no_ext, ext = os.path.splitext(file_path)
    im = Image.open(file_path)
    grey = im.convert('L')
    reshaped = np.asarray(grey.getdata(), dtype=np.float64).reshape((grey.size[1], grey.size[0]))
    sig = np.sum(reshaped, axis=1)
    prev_cut = 0
    for i, cut in enumerate(get_cuts_mid_tresh(sig)):
        cropped = grey.crop((0, prev_cut, grey.size[0], cut))
        prev_cut = cut
        cropped.save('{}_slip_{}{}'.format(no_ext, i, ext))

if __name__ == '__main__':
    for root, dirnames, filenames in os.walk(IMG_ROOT):
        for filename in fnmatch.filter(filenames, IMG_GLOB):
            split_to_lines(os.path.join(root, filename))
