# README #

### Opis ###

Temat: Aplikacja do rozliczania paragonów jako przykład ilustrujący problem digitalizacji dokumentów. (An application for accounting receipts as an example illustrating the problem of document digitization.)

Proces digitalizacji dokumentów w dzisiejszych czasach wciąż jest dosyć złożoną kwestią, która zależy od wielu czynników.

Jakość użytych zdjęć czy różnorodność danych w dokumentach może przyczynić się do problemów w rozpoznawaniu faktur czy paragonów.

Niniejsza praca ma na celu przedstawić problem digitalizacji dokumentów poprzez omówienie opracowanej aplikacji automatyzującej proces rozliczania wspólnych paragonów.

Opracowana aplikacja powinna umożliwiać wprowadzanie zdjęć paragonów do systemu i odczyt znajdujących się na nich informacji, a następnie obliczać podział kosztów między osobami korzystającymi z systemu.

### Stos technologiczny ###

* aplikacja webowa
* front-end: reactjs, react-router, react-redux, npm, webpack
* back-end: php7, composer
* biblioteka do rozpoznawania obrazów (OCR): tesseract-ocr (jako biblioteka do PHP)

### Wlasna siec neuronowa do rozpoznawania liter ###

* https://www.mathworks.com/help/nnet/gs/classify-patterns-with-a-neural-network.html?requestedDomain=www.mathworks.com